## TASKS
-------------------------------------------------------------

Prepare a Web interface that displays the content pulled from the feeds in a table format.
You should consume a minimum of 4 feeds and limit the API calls to 10 or under, the deadline is noon on Thursday.

## HOW THE PROJECT WAS DONE
-------------------------------------------------------------

This is a Single Page Web Interface that collects data from the Sportradar API 
The web interface is written with PHP and HTML using the Bootstrap Framework
PHP snippets are used to populate the Bootstrap HTML Tables 

## HOW TO GET THE PROJECT UP AND RUNNING LOCALLY
-------------------------------------------------------------

To run the page locally on your pc 

	* Make sure you have XAMPP installed and an active internet connection 
	* Clone or download the zip file from Bitbucket 
	* Extract the folder and place it in this directory C:/xampp/htdocs
	* Run your XAMPP and start your Apcahe and MySQL 
	* Open your browser and navigate to your localhost/fuzzy-octo-palm-tree/views/index.html
	* You should be able to see the Web Interface