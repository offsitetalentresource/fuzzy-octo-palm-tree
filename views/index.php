<?php
//gets the api for tournament standings and assigns it to a variable
$data_url = 'http://api.sportradar.us/soccer-t3/eu/en/tournaments/sr:tournament:17/standings.json?api_key=zcqfwkst22vxy5wburw4qq2e';

//$daily_data = 'http://api.sportradar.us/soccer-t3/eu/en/schedules/[year]-[month]-[day]/schedule.json?api_key=zcqfwkst22vxy5wburw4qq2e';

//gets the api for tournament results and assigns it to a variable 
$tournament_result = 'http://api.sportradar.us/soccer-t3/eu/en/tournaments/sr:tournament:17/results.json?api_key=zcqfwkst22vxy5wburw4qq2e';

//gets the api for tournament schedule and assigns it to a variable
$tournament_shdl = 'http://api.sportradar.us/soccer-t3/eu/en/tournaments/sr:tournament:17/schedule.json?api_key=zcqfwkst22vxy5wburw4qq2e';

//gets the api for player rankings and assigns it to a variable
$player_ranking = 'http://api.sportradar.us/soccer-t3/eu/en/tournaments/sr:tournament:17/leaders.json?api_key=zcqfwkst22vxy5wburw4qq2e';

//gets data for tournament standing 
$data_get = file_get_contents($data_url);
$data_array = json_decode($data_get, true);
//gets data for tournament results
$tr_get = file_get_contents($tournament_result);
$tr_array = json_decode($tr_get, true);
//gets data for tournament schedule
$ts_get = file_get_contents($tournament_shdl);
$ts_array = json_decode($ts_get, true);
//gets data for player ranking
$pr_get = file_get_contents($player_ranking);
$pr_array = json_decode($pr_get, true);

//assigns the data gotten from the json file to variables that can be interfaced with the html
$table = $data_array['standings'][0]['groups'][0];
$table_2 = $tr_array;
$table_3 = $ts_array;
$table_4 = $pr_array;

?>

<!--Bootstrap HTML-->
<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sportradar API Interface</title>
    
    <!--Bootstrap Dependencies(CSS)-->
    <link rel="shortcut icon" href="">
    <link rel="stylesheet" href="../public/lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../public/lib/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    
    <!--Stylesheet-->
    <link rel="stylesheet" href="../public/css/style.css">
    <!--[if IE]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!--The Navigation Bar-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">SportsRadar</a>
            </div><!--navabar-header-->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div>
        </div><!--container-->
    </nav><!--navbar-->
    
    <!--Tournament League Standing-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <h4 class="text-center">2016/2017 LEAGUE STANDING</h4>
                </ul>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Rank</th>
                            <th>Team</th>
                            <th>Played</th>
                            <th>Win</th>
                            <th>Draw</th>
                            <th>Loss</th>
                            <th>GF</th>
                            <th>GA</th>
                            <th>GD</th>
                            <th>Pts</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            //PHP snippet for populating the tournament standings table 
                            foreach($table['team_standings'] as $table_data){
                            echo "<tr>
                                    <td>{$table_data['rank']}</td>
                                    <td>{$table_data['team']['name']}</td>
                                    <td>{$table_data['played']}</td>
                                    <td>{$table_data['win']}</td>
                                    <td>{$table_data['draw']}</td>
                                    <td>{$table_data['loss']}</td>
                                    <td>{$table_data['goals_for']}</td>
                                    <td>{$table_data['goals_against']}</td>
                                    <td>{$table_data['goal_diff']}</td>
                                    <td>{$table_data['points']}</td>
                                </tr>";
                            }
                        ?>
                    </tbody>
                </table><!--table table-striped-->
            </div>
        </div>
    </div><!--container-->
    
    <!--Tournament League Schedule-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                        <h4 class="text-center">2016/2017 UPCOMING FIXTURES</h4>
                </ul>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Home Team</th>
                            <th>VS</th>
                            <th>Away Team</th>
                            <th>Schedule</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            //PHP snippet for populating the tournament schedule table
                            foreach($table_3['sport_events'] as $table_data2){
                                echo "<tr>
                                        <td>{$table_data2['competitors'][0]['name']}</td>
                                        <td>VS</td>
                                        <td>{$table_data2['competitors'][1]['name']}</td>
                                        <td>{$table_data2['scheduled']}</td>
                                    </tr>";

                            }
                        ?>
                    </tbody>
                </table><!--table table-striped-->
            </div>
        </div>
    </div><!--container-->
    
    <!--Tournament League Results -->
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <h4 class="text-center">2016/2017 LEAGUE RESULTS</h4>
                </ul>
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Home Team</th>
                            <th>vs</th>
                            <th>Away Team</th>
                            <th>Score Line</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            //PHP snippet for populating the tournament result table
                            foreach($table_2['results'] as $table_data1){
                            echo "<tr>
                                    <td>{$table_data1['sport_event']['competitors'][0]['name']}</td>
                                    <td>VS</td>
                                    <td>{$table_data1['sport_event']['competitors'][1]['name']}</td>
                                    <td>
                                        {$table_data1['sport_event_status']['home_score']}&#x0020:&#x0020
                                        {$table_data1['sport_event_status']['away_score']}
                                    </td>
                                </tr>";
                            }
                        ?>
                    </tbody>
                </table><!--table table-striped-->
            </div>
            <div class="col-md-6">
                <ul class="breadcrumb">
                        <h4 class="text-center">2016/2017 LEAGUES GOALS RANKINGS</h4>
                </ul>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Team</th>
                            <th>Player</th>
                            <th>Goals</th>
                            <th>Assists</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            //PHP snippet for populating the player rankings table
                            foreach($table_4['top_points'] as $table_data3){
                                echo "<tr>
                                        <td>{$table_data3['team']['name']}</td>
                                        <td>{$table_data3['player']['name']}</td>
                                        <td>{$table_data3['goals']}</td>
                                        <td>{$table_data3['assists']}</td>
                                    </tr>";

                            }
                        ?>
                    </tbody>
                </table><!--table table-striped-->
            </div>
        </div>
    </div><!--container-->
    
    <!--Bootstrap Dependencies(JavaScript)-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="../public/lib/jquery/jquery-1.12.2.min.js"></script>
    <script src="../public/lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>